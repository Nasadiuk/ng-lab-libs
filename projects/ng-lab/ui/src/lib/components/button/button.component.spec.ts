import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ButtonComponent } from "./button.component";

describe("ButtonComponent", () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let buttonElement: HTMLButtonElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    buttonElement = fixture.debugElement.nativeElement.querySelector("button");
  });

  for (const type of [undefined, null, "123"]) {
    it("should throw if no or invalid type was passed", () => {
      component.type = <any>type;
      expect(() => component.buttonType).toThrow();
    });
  }

  for (const type of ["primary", "secondary"]) {
    it("should create if valid type was passed", () => {
      component.type = <any>type;
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });
  }

  it("emits event on button click", done => {
    component.type = "primary";
    fixture.detectChanges();
    spyOn(component, "handleClick");

    buttonElement.click();

    fixture.whenStable().then(() => {
      expect(component.handleClick).toHaveBeenCalled();
      component.onClick.subscribe(value => expect(value).toEqual(undefined));
      done();
    });
  });

  it("does not execute method on disabled button", done => {
    component.type = "primary";
    component.disabled = true;
    fixture.detectChanges();
    spyOn(component, "handleClick");

    buttonElement.click();

    fixture.whenStable().then(() => {
      expect(component.handleClick).not.toHaveBeenCalled();
      done();
    });
  });
});
