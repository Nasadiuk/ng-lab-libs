## Buttons
NG lab libs buttons are native `<button>` elements enhanced 
with Material Design styling and ink ripples.

### HTML code
```html
<ui-button (onClick)="handleClick($event)" [type]="'accent'">Click Me!</ui-button>
```

### Typescript code

```ts
public handleClick(event: MouseEvent): void {
    // your awesome code here
}
```



