import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ButtonType } from "../../enums/button-type.enum";

@Component({
  selector: "ui-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"],
})
export class ButtonComponent {
  @Input()
  public type: ButtonType;

  @Input()
  public disabled: boolean;

  @Output()
  public onClick = new EventEmitter<MouseEvent>();

  /**
   * Handles click on html button
   */
  public handleClick($event: MouseEvent): void {
    this.onClick.emit($event);
  }

  get buttonType(): string {
    if (!ButtonType[this.type]) {
      throw new Error("Invalid button type");
    }

    return ButtonType[this.type];
  }
}
