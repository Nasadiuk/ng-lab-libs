import { moduleMetadata, storiesOf } from "@storybook/angular";
import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { ButtonComponent } from "./button.component";
import { action } from "@storybook/addon-actions";
// @ts-ignore
import markdownNotes from './button.stories.md';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
  template: `
      <ui-button (onClick)="handleClick($event)" [type]="'accent'">Click Me!</ui-button>
  `,
})
class ButtonViewComponent {
  public handleClick(event: MouseEvent): void {
    action("Action value: ")(event);
  }
}

// @ts-ignore
storiesOf("Buttons", module)
  .addDecorator(
    moduleMetadata({
      declarations: [ButtonViewComponent, ButtonComponent],
      imports: [CommonModule],
    }),
  )
  .add("Accent button", () => ({
    component: ButtonViewComponent,
    props: {
        type: "accent",
      },
  }),
  {
    notes: { markdown: markdownNotes },
  });
