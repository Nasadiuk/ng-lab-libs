export enum ButtonType {
  colored = "mdl-button mdl-js-button mdl-button--raised mdl-button--colored",
  accent = "mdl-button mdl-js-button mdl-button--raised mdl-button--accent",
  accentRipple = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent",
}
