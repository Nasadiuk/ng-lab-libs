# Ng-lab UI library

This library contains custom Angular UI components based on **Material Design Lite**.

**Available components:**

- Button

**Available properties:**

- type
- disabled

**Available button types:**

- colored
- accent
- accentRipple

**Getting started:**
Add _UiModule_ to your application _AppModule_

**Usage example:**

```html
<ui-button [type]="'accentRipple'">Click Me</ui-button>
```
